<?php

namespace App\Controller;

use App\Entity\Beacon;
use App\Service\BeaconService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class BeaconController extends AbstractController
{
    #[Route('/getBubbleFromBeacon', name: 'getBubbleFromBeacon')]
    public function getAvailableBeacons(Request $request, BeaconService $service,EntityManagerInterface $em): Response
    {
        $clientCode = $request->query->get('code');

        $response = array();
        $repository = $em->getRepository('App:Beacon');
        $listBeacons = $repository->findAll();

        foreach($listBeacons as $beacon) {
            if ($beacon->getCode() == $clientCode) {
                $beaconResponse = array('name'=>$beacon->getName(),
                                    'key'=>$beacon->getBulle());
                array_push($response, $beaconResponse);
            }
        }

        return new JsonResponse($response);
    }

    #[Route('/addBeacon', name: 'addBeacon')]
    public function addBeacon(Request $request, BeaconService $service) : Response
    {
        $clientName = $request->query->get('name');
        $clientCode = $request->query->get('code');
        $clientIdBubble = $request->query->get('idBubble');

        $beacon = $service->createBeacon($clientName, $clientCode, $clientIdBubble);
        return new JsonResponse($beacon);
    }
}


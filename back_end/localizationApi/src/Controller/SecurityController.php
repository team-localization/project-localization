<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function login(Request $request): JsonResponse
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserService $userService, UserPasswordEncoderInterface $passwordEncoder){
        $array = $request->toArray();
        $user = new User();
        $user->setUsername($array['username']);
        $user->setPassword($passwordEncoder->encodePassword($user,$array['password']));
        $userService->add($user);
        return new JsonResponse('ok');
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout():void
    {
    }
}
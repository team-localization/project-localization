<?php
namespace App\Service;


use App\Entity\Beacon;
use Doctrine\ORM\EntityManagerInterface;

class BeaconService extends BaseService{

  public function __construct(EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager);
  }

  public function createBeacon($name, $code, $idBubble){
      $beacon = new Beacon();
      $beacon->setName($name);
      $beacon->setCode($code);
      $beacon->setIdBubble($idBubble);
      $this->add($beacon);
      return $beacon;
  }
  
  public function getAllBeacons(){
      return $this->em->getRepository('App:Beacon')->findAll();
  }

  public function findById($id){
      return $this->em->getRepository('App:Beacon')->find($id);
  }

  public function updateBeacon($beacon, $name, $code, $idBubble){
      $beacon->setName($name);
      $beacon->setCode($code);
      $beacon->setIdBubble($idBubble);
      $this->update();
      return $beacon;
  }
}
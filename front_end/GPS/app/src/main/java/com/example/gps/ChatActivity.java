package com.example.gps;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.ale.infra.list.IItemListChangeListener;
import com.ale.infra.manager.IMMessage;
import com.ale.infra.manager.room.Room;
import com.ale.infra.proxy.conversation.IRainbowConversation;
import com.ale.rainbowsdk.RainbowSdk;
import com.google.android.gms.location.LocationServices;

import java.util.List;

public class ChatActivity extends AppCompatActivity {

    private ListView m_listViewMessages;
    private EditText m_textMessage;
    private ImageButton m_buttonSend;
    private Room m_room;
    private IRainbowConversation m_conversation;
    private MessageAdapter m_messageAdapter;
    private IItemListChangeListener m_messageChangeListener;
    public String m_roomId;

    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    private LocalizationService m_localService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        m_listViewMessages = findViewById(R.id.listViewMessages);
        m_textMessage = findViewById(R.id.textMessage);
        m_buttonSend = findViewById(R.id.btnSendMessage);
        m_buttonSend.setEnabled(false);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null)
        {
            m_roomId = bundle.getString(MainActivity.RAINBOW_ROOM);
            m_room = RainbowSdk.instance().bubbles().findBubbleById(m_roomId);
            if(m_room != null)
            {
                setupActivity();
                setTitle(m_room.getName());
            }
        }
        LocalizationService serv = new LocalizationService(this);
        serv.GoogleApiClient.connect();
        m_localService = serv;
    }

    private void setupActivity()
    {
        m_buttonSend.setEnabled(true);

        Log.v("ChatActivity", m_textMessage.toString());
        m_conversation = RainbowSdk.instance().conversations().getConversationFromRoom(m_room);

        m_messageAdapter = new MessageAdapter(this);
        m_listViewMessages.setAdapter(m_messageAdapter);

        m_messageChangeListener = () -> {
            RefreshMessages();
        };

        m_conversation.getMessages().registerChangeListener(m_messageChangeListener);

        loadMessages();
        RefreshMessages();
    }

    // This method will load the 50 last messages
    private void loadMessages() {
        RainbowSdk.instance().im().getMessagesFromConversation(m_conversation); // Default 50 messages
    }

    // If you want to get more messages
    public void loadMoreMessage(View view) {
        RainbowSdk.instance().im().getMoreMessagesFromConversation(m_conversation); // Get 50 more messages
    }

    private void RefreshMessages() {
        List<IMMessage> messages =  m_conversation.getMessages().getCopyOfDataList();
        runOnUiThread(() -> {
            m_messageAdapter.m_messages.clear();
            m_messageAdapter.addAll(messages);
        });

        m_listViewMessages.setSelection(m_messageAdapter.getCount() - 1);
    }

    public void onSendMessageButton(View view) {
        String message = m_textMessage.getText().toString();
        if(message.length() > 0)
        {
            RainbowSdk.instance().im().sendMessageToConversation(m_conversation, message);
            m_textMessage.setText("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        m_conversation.getMessages().registerChangeListener(m_messageChangeListener); // Register listener first
        loadMessages(); // load messages

        if (!m_localService.checkPlayServices()) {
            Log.d("ERROR","You need to install Google Play Services to use the App properly");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        m_conversation.getMessages().unregisterChangeListener(m_messageChangeListener);

        // stop location updates
        if (m_localService.GoogleApiClient != null  &&  m_localService.GoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(m_localService.GoogleApiClient, m_localService);
            m_localService.GoogleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : m_localService.PermissionsToRequest) {
                    if (!m_localService.HasPermission(perm)) {
                        m_localService.PermissionsRejected.add(perm);
                    }
                }

                if (m_localService.PermissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(m_localService.PermissionsRejected.get(0))) {
                            new AlertDialog.Builder(this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(m_localService.PermissionsRejected.
                                                        toArray(new String[m_localService.PermissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }

                } else {
                    if (m_localService.GoogleApiClient != null) {
                        m_localService.GoogleApiClient.connect();
                    }
                }

                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (m_localService.GoogleApiClient != null) {
            m_localService.GoogleApiClient.connect();
        }
    }

    public void exitBubble() {
        //finish();
    }
}



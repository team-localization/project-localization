package com.example.gps;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LocalizationService  implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Location m_location;
    public String BubbleId;
    public String BubbleName;
    //TO DO: retirer le cast brut et remplacer par une façon "propre" pour les deux activités
    private Activity m_activity;

    // lists for permissions
    public ArrayList<String> PermissionsToRequest;
    public ArrayList<String> PermissionsRejected = new ArrayList<>();
    public ArrayList<String> Permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    public GoogleApiClient GoogleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest m_locationRequest;

    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds

    public LocalizationService(Activity activity){
        m_activity = activity;
        initPermissions();
        initGoogleApi();
        //updateCoordinates();
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        m_location = location;
        UpdateCoordinates();
    }

    private void UpdateCoordinates() {
        checkBubble();

        if(m_location != null) {
            if(m_activity.getClass() == MainActivity.class) {
                ((MainActivity) m_activity).UpdateCoordinates();
            }
            else if(m_activity.getClass() == ChatActivity.class && (BubbleId != null && !BubbleId.equals(((ChatActivity) m_activity).m_roomId))){
                ((ChatActivity) m_activity).exitBubble();
            }
        }
    }

    public void checkBubble(){
        RequestQueue queue = Volley.newRequestQueue(m_activity);
        String url = "http://54.37.152.21:3000/zonesByRadius?latitude=" + m_location.getLatitude() +
                "&longitude=" + m_location.getLongitude();


        StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                response -> {
                    JSONObject bubble;
                    JSONArray bubbleArray;
                    try {
                        bubbleArray = new JSONArray(response);
                        bubble = bubbleArray.getJSONObject(0);
                        String bubbleFromResponse = bubble.getString("key");
                        if(BubbleId  == null || !BubbleId.equals(bubbleFromResponse)){

                            BubbleName = bubble.getString("name");
                            BubbleId = bubble.getString("key");

                            if(m_activity.getClass() == MainActivity.class) Toast.makeText(m_activity,"Vous venez d'entrez dans la zone de la bulle "+ BubbleName, Toast.LENGTH_SHORT).show();
                        }
                        else if(bubble.getString("key").equals("")) BubbleId = "";


                    } catch (JSONException e) {
                        e.printStackTrace();
                        BubbleId = "";
                    }

                },
                error -> Log.v("ERROR HTTP", error.toString()));
        queue.add(stringRequest);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(m_activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(m_activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        m_location = LocationServices.FusedLocationApi.getLastLocation(GoogleApiClient);

        startLocationUpdates();
    }

    private void startLocationUpdates() {
        m_locationRequest = new LocationRequest();
        m_locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        m_locationRequest.setInterval(UPDATE_INTERVAL);
        m_locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(m_activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(m_activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(m_activity, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(GoogleApiClient, m_locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    private void initPermissions(){
        // we add permissions we need to request location of the users
        Permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        Permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        PermissionsToRequest = permissionsToRequest(Permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionsToRequest.size() > 0) {
                m_activity.requestPermissions(PermissionsToRequest.
                        toArray(new String[PermissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!HasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean HasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return m_activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }



    private void initGoogleApi(){
        GoogleApiClient = new GoogleApiClient.Builder(m_activity).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }

    public String getLatitude(){
        return String.valueOf(m_location.getLatitude());
    }

    public String getLongitude(){
        return String.valueOf(m_location.getLongitude());
    }

    boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(m_activity);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(m_activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                m_activity.finish();
            }

            return false;
        }

        return true;
    }
}

 package com.example.gps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;


public class MainActivity extends AppCompatActivity implements BeaconConsumer {

    private TextView m_lat;
    private TextView m_long;
    private TextView m_textbeacon;
    private Button m_bubbleButton;

    private String m_bubbleId;
    private String m_bubbleIdBeacon;

    private String m_bubbleName;
    private Button m_bubbleButtonBeacon;


    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    private LocalizationService m_localService;

    public static final String RAINBOW_ROOM = "room";
    private static final String TAG = "ROOM";

    private BeaconManager beaconManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_localService = new LocalizationService(this);

        initViews();
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.bind(this);
        m_textbeacon = findViewById(R.id.beacon_txt);


    }


    public void UpdateCoordinates(){
        m_long.setText("Longitude: " + m_localService.getLongitude());
        m_lat.setText("Latitude: "+ m_localService.getLatitude());
        m_bubbleButton.setText("Rejoindre la bulle " + m_localService.BubbleName);
        m_bubbleButton.setVisibility(m_localService.BubbleName != null ? View.VISIBLE : View.GONE);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : m_localService.PermissionsToRequest) {
                    if (!m_localService.HasPermission(perm)) {
                        m_localService.PermissionsRejected.add(perm);
                    }
                }

                if (m_localService.PermissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(m_localService.PermissionsRejected.get(0))) {
                            new AlertDialog.Builder(this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(m_localService.PermissionsRejected.
                                                        toArray(new String[m_localService.PermissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }

                } else {
                    if (m_localService.GoogleApiClient != null) {
                        m_localService.GoogleApiClient.connect();
                    }
                }

                break;
        }
    }

    private void initViews() {
        setContentView(R.layout.activity_main);

        m_lat = findViewById(R.id.tv_latitude);
        m_long = findViewById(R.id.tv_longitude);

        m_bubbleButton = findViewById(R.id.button_enter_bubble);
        m_bubbleButtonBeacon = findViewById(R.id.button_enter_bubble_beacon);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (m_localService.GoogleApiClient != null) {
            m_localService.GoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!m_localService.checkPlayServices()) {
            Log.d("ERROR", "You need to install Google Play Services to use the App properly");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        /*if (m_localService.GoogleApiClient != null  &&  m_localService.GoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(m_localService.GoogleApiClient, m_localService);
            m_localService.GoogleApiClient.disconnect();
        }*/
    }





    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
    }




    public void checkBubbleBeacon(String idBulle){
        //Log.i(TAG, "test checkBubble");
        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "http://54.37.152.21:3000/getBubbleFromBeacon?code=" + idBulle;


        StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                response -> {
                    JSONObject bubble;
                    JSONArray bubbleArray;
                    try {
                        bubbleArray = new JSONArray(response);
                        bubble = bubbleArray.getJSONObject(0);
                        if(m_bubbleId == null || !m_bubbleId.equals(bubble.getString("key"))){

                            m_bubbleName = bubble.getString("name");
                            m_bubbleId = bubble.getString("key");

                            m_bubbleIdBeacon = m_bubbleId;
                            Toast.makeText(MainActivity.this, "Vous venez d'entrez dans la zone de la bulle associé au Beacon " + m_bubbleName, Toast.LENGTH_SHORT).show();

                        }
                        m_bubbleButtonBeacon.setText("Rejoindre la bulle Beacon " + m_bubbleName);
                        //Log.v(TAG,"testButton0");
                        m_bubbleButtonBeacon.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                error -> Log.v("ERROR HTTP", error.toString()));
        queue.add(stringRequest);
    }



    public void enterBubble(View view) {

        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        intent.putExtra(RAINBOW_ROOM, m_bubbleIdBeacon);
        startActivity(intent);
    }

    public void enterBubbleBeacon(View view) {
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        intent.putExtra(RAINBOW_ROOM, m_bubbleIdBeacon);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {

                //String id = "759d2e0a-aaee-4f0c-b767-a1a346b89400";
                //checkBubbleBeacon(id);
                if (beacons.size() > 0) {
                    Beacon bc = beacons.iterator().next();
                    Log.i(TAG, "The first beacon I see is about "+ bc.getDistance()+" meters away.");

                    Log.i(TAG, "ID1 : " + bc.getId1());
                    m_textbeacon.setText(Double.toString(bc.getDistance()) + "\n" + bc.getId1());
                    checkBubbleBeacon(bc.getId1().toString());
                    // m_beaconRequest.postBeacon(beacons.iterator().next().getId1().toString(), m_email);


                } else {

                    m_textbeacon.setText("Aucun beacon détecté, veuillez laisser tourner l'application en arrière plan.");
                }
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));

        } catch (RemoteException e) {    }

    }
}
package com.example.geocaching;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ale.listener.SigninResponseListener;
import com.ale.listener.StartResponseListener;
import com.ale.rainbowsdk.RainbowSdk;


public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_QUEST = "Selected quest" ;
    private ListView _questsList;
    private ArrayAdapter<Quest> _questsAdapter;
    private List<Quest> _quests = new ArrayList<Quest>();
    private EditText m_textEmail;
    private EditText m_textPassword;
    private Button m_connectButton;
    private QuestRPC m_QuestRPC;


    public static final String RAINBOW_ROOM = "room";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new QuestRPC(this).execute(1);

        _questsList = findViewById(R.id.questsList);
        _questsAdapter = new ArrayAdapter<Quest>(this, android.R.layout.simple_list_item_1, _quests);
        _questsList.setAdapter(_questsAdapter);

        _questsList.setOnItemClickListener((parent, view, position, id) -> {
            Quest selectedQuest = _quests.get(position);
            Intent intent = new Intent(MainActivity.this, HintsActivity.class);
            intent.putExtra(EXTRA_QUEST, selectedQuest);
            startActivity(intent);
        });

        m_textEmail = findViewById(R.id.textEmail);
        m_textPassword = findViewById(R.id.textPassword);
        m_connectButton = findViewById(R.id.connectButton);

        //admin user.admin@mycompany.com Adm1npwd! Admin Admin
        m_textEmail.setText("test.user@mycompany.com");
        m_textPassword.setText("Sup3rpwd!");

        RainbowSdk.instance().initialize(this, "4425bdb0a5e511ebbe33e9ed28980ec8", "Y7Q3TRKfbaM1ZQkn3gb5Uzdyh3etUgm9bLTPWYiLG8zXhL4Z3JDBrVNCLC4IKpTb");

        RainbowSdk.instance().connection().start(new StartResponseListener() {
            @Override
            public void onStartSucceeded() {
                // sign in with one of the following method
                Log.v("MainActivity", "start : success");
            }

            @Override
            public void onRequestFailed(RainbowSdk.ErrorCode errorCode, String err) {
                // Something was wrong
                Log.v("MainActivity", "start : fail " + errorCode + " " + err);
            }
        });
    }

    public void onConnectButton(View view) {

        RainbowSdk.instance().connection().signin(m_textEmail.getText().toString(), m_textPassword.getText().toString(), "sandbox.openrainbow.com", new SigninResponseListener() {
            @Override
            public void onSigninSucceeded() {
                // You are now connected to the production environment
                // Do something on the thread UI
                //String roomId = "608ac3dbf2136d63a67e8071";
                String roomId = "5c1fccbeef114a7a8ea0073462ab5ae4";
                /*Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                intent.putExtra(RAINBOW_ROOM, roomId);
                startActivity(intent);*/
            }

            @Override
            public void onRequestFailed(RainbowSdk.ErrorCode errorCode, String err) {
                // Do something on the thread UI
                Log.v("MainActivity", "signin :fail " + errorCode + " " + err);
            }
        });
    }

    public void populate(List<Quest> quests){
        _quests.addAll(quests);
        _questsAdapter.notifyDataSetChanged();
    }
}

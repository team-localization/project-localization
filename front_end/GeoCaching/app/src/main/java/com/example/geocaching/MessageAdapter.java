package com.example.geocaching;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ale.infra.contact.IRainbowContact;
import com.ale.infra.manager.IMMessage;
import com.ale.rainbowsdk.RainbowSdk;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MessageAdapter extends BaseAdapter {
    List<IMMessage> m_messages = new ArrayList<IMMessage>();
    Context m_context;

    public MessageAdapter(Context context) {
        m_context = context;
    }

    public void add(IMMessage message) {
        m_messages.add(message);
        notifyDataSetChanged();
    }

    public void addAll(Collection<IMMessage> messages) {
        m_messages.addAll(messages);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return m_messages.size();
    }

    @Override
    public Object getItem(int i) {
        return m_messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MessageViewHolder holder = new MessageViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) m_context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        IMMessage message = m_messages.get(i);

        if (message.isMsgSent()) {
            convertView = messageInflater.inflate(R.layout.my_message, null);
            holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
            holder.dateMessage = (TextView) convertView.findViewById(R.id.dateMessage);
            convertView.setTag(holder);
            
            holder.messageBody.setText(message.getMessageContent());
            holder.dateMessage.setText(DateFormat.getDateInstance(DateFormat.LONG).format(message.getMessageDate()));

        } else {
            convertView = messageInflater.inflate(R.layout.other_message, null);
            holder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
            holder.dateMessage = (TextView) convertView.findViewById(R.id.dateMessage);
            convertView.setTag(holder);

            IRainbowContact contact = RainbowSdk.instance().contacts().getContactFromJabberId(message.getContactJid());
            holder.name.setText(contact.getFirstName() + " " + contact.getLastName());
            holder.messageBody.setText(message.getMessageContent());
            holder.avatar.setImageBitmap(contact.getPhoto());
            if(contact.getPhoto() == null)
            {
                holder.avatar.setBackgroundColor(Color.argb(255,70,130,180));
            }
            holder.dateMessage.setText(DateFormat.getDateInstance(DateFormat.LONG).format(message.getMessageDate()));
        }

        return convertView;
    }

    class MessageViewHolder {
        public ImageView avatar;
        public TextView name;
        public TextView messageBody;
        public TextView dateMessage;
    }

}
